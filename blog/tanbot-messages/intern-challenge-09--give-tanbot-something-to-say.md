# Week 9: Give Tanbot Something to Say

#### _`date_scheduled = datetime(2021, 3, 20, 20, 30, 0)`_

#### _`title = 'broadcast'`_

This is Week 9 -- the second-to-last week of your internship! 
Now that you've had to endure all these naggy messages for 9 weeks, I thought you might like to see how Tanbot works.
The assignment this week is to try your hand at giving Tanbot something to say.

Here are the keys to the Tanbot kingdom:

admin interface: http://staging.qary.ai/admin/
username: Tanbot
password: tangible1

Once you're logged in you can see all the tables of data that Tanbot uses to do its work. Explore around and see if you can figure out what each one is for.  And here's your mission, should you chose to accept it: [Give Tanbot Something Interesting to say](gitlab.com/tangibleai/team/-/blob/master/src/team/blog/tanbot-messages/intern-challenge-09--give-tanbot-something-to-say-details.md) 

