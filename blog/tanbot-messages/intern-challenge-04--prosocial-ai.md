# intern-challenge-04 prosocial ai

While you're waiting for this week's lesson on word vectors (word embeddings), you can get started on the bonus assignment... researching and supporting prosocial ai.

Find futurists, technologists, researchers, and philosophers that are worried about the AI control problem or the technological singularity or the economic singularity. Terms like "loyal ai" vs "beneficial ai" and AGI might be good searches in a prosocial search engine like Duck.com or scholar.google.com. You will probably enjoy connecting to the low level, marginalized engineers who are getting down to work building open source packages that democratize AI and nudge it to be more prosocial. Try to follow at least 10 such people on GitLab, reddit, twitter, linked in, or github (in order from least antisocial business to more antisocial). And share your finds on Slack

## Humans to watch

The following are some prosocial ai researchers, teachers, engineers, sociologists, and influencers that stood out in the past few months:

- Zeynep Tufekci
- Melanie Mitchel
- Kathryn Soo McCarthy
- Martin Nowak & Roger Highfield
- Stuart Russel (_AIMA_ and _Human Compatible_)
- Peter Norvig?
- Helen Pluckrose & Peter Boghossian
- Rob Reid
- some of Lex Fridman's guests
- Silva Micali (Turing Award winner, Algorand cryptocurrency inventor)
- Marcus Hutter
- Susan Schneider (_Artificial You_)
- Joy Buolamwini (Coded Bias on Netflix)

Try to find related, but less famous people, so that you get the most prosocial leverage for you likes.

## Organizations (algorithms) to Watch

And here some prosocial organizations that might be good for your prosocial search:

- Omdena
- Data Kind
- Dimagi
- Plan International
- Open Science Foundation
- fsf.org
- eff.org
- osf.io
