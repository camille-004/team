# intern-challenge-05 nessvectors

This week you'll learn about **nessvectors**. 
Nessvectors are word embeddings or vectors, such as word2vec or GloVe, that have been simplified to ignore everything except the components or dimensions you care about.

This [package](https://gitlab.com/tangibleai/nessvec) will show you how. 

### Get started
 
`pip install nessvec`
OR
Clone the repository and install it from the source code. 

Play around with the examples in the `README.md`.
Find a couple titles of 2 news articles and find out their nessvectors. 
You can examine the politicalness and scienceness and kindness and femaleness of the words in the article title.

### BONUS: 
Create a yaml file with a list of lists. Each sublist should have 5 elements. The titles of your articles should be the first element (column) of you sublists. And the 4 scores for scienceness, femaleness, etc should be float values in the remaining sublist elements. Push your examples to gitlab and merge request them into the nessvec repo so we can use them as examples in the book.

### SUPERBONUS: help the other interns do all this and consolidate your contributions into a single yaml file.
