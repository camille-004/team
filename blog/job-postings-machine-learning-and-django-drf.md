# Django and ML Engineers Wanted

We're kicking off 2 small projects and need a Python and an NLP engineer this quarter. We hope to start onboarding the first engineer in two weeks. And will likely offer additional opportunities in the coming months. Junior developers are welcome all new developers are paired with senior team members. And all of Tangible AI's work is for social-impact organizations such as nonprofits and charities.

#### Backend Web Dev:
  * Build API (Django+DRF) integrated with a digital health system
  * compensation: ~$10k over ~3 mo at 20-40 hr/week

#### Machine Learning Engineer:
  * Text mining for and data science for education sector data
  * compensation: ~$10k over ~3 mo at 20-40 hr/week

If you have successfully deployed a Django application of your own design and understand how chatbots work (by participating in the [Qary](https://gitlab.com/tangibleai/qary) community), you may be an ideal candidate for the Backend Web Dev opening.

If you have completed a Data Science bootcamp or a Tangible AI internship, you likely have what it takes for the Machine Learning Engineer role.

The ideal candidates will have some of the following skills and interest in learning the others. We will help you level up.

Please e-mail us at engineering@tangibleai.com with the role(s) you are interested in. And you can get a head start on the application process if you self-rate yourself on the skills below within the text of your e-mail to us. And we'd love to hear about the skills or projects you are most proud of. If your email is machine readable as well as human-readable you get bonus points.

* [  ] Written communication skill in English (1=poor, 5=published author)
* [  ] Verbal communication in English (1=difficult, 5=renowned speaker/facilitator)
* [  ] Prosocial (1=egocentric, 3=empathetic, 5=altruistic)
* [  ] Resilience (1=defensive, 5=proactively seeks feedback)
* [  ] Independence (1=unreliable, 5=completed many hard projects)
* [  ] Teamwork (1=adversarial, 2=critical, 3=cooperative, 4=encouraging, 5=inspiring)
* [  ] Conversation design (1=never done it, 3=personal proj., 5=10k+ users)
* [  ] Chatbot frameworks (Rasa, Botpress, Landbot.io, Qary)
* [  ] Webdev, backend (Django, Flask, REST APIs)
* [  ] Data (Pandas, SQL, Postgres, ElasticSearch, csv, yaml, json)
* [  ] Bash/Linux (curl, wget, ls, cd, cat, grep, find, ssh)
* [  ] Devops (Docker, ssh, DigitalOcean, DNS, Firewalls, conda, pip, poetry)
* [  ] Machine Learning (pandas, sklearn, torch, tensorflow)
* [  ] Python (classes, str interpolation, functions, loops, conditionals, regex, recursion, decorators, comprehension, generators, iterables, types, dict, zip)
* [  ] Computer Science (algorithms, complexity, big O, separation of concerns, modularity, DRY, refactoring, parameterization, kwargs, args, cli args)
* [  ] Agile (user stories, TDD, DDD, coverage, estimation, standups, MVP)
* [  ] Version control (gitlab, git, diff, meld, merge, code review)
