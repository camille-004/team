# A Sublime Workflow

I love Sublime Text. It doesn't have the most features (but it does have a lot of great plugins). It's not supported by a community of open source developers or some big tech company. But it has a fully customizable interface. That customizability can be a bit daunting for a new developer. So here are some tips to get up and running quickly. And some mentees have put together videos to help you get started, if you're like "where's the TLDR."


## Quick Start

1. [Install Sublime Text](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-install-sublime-text-and-start-new-project-mac-osx-add-ssh-key-to-gitlab.mp4) 15 min video y Eda Firat
2. [Create your first Sublime Project](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-sublime-text-start-software-project-2020-10-20intern-eda-howto-sublime-start-a-new-project.mp4) 15 min video by Eda Firat

1. install [sublime text 3](https://www.sublimetext.com/)
  - download and launch the installer (may say "upgrade" in Ubuntu if you used Sublime Text 2 before)
  - [buy a license](https://www.sublimetext.com/buy) to support an awesome developer
2. install [package manager](https://packagecontrol.io/installation)
  - copy the python one-liner shown for sublime text 3
  - in sublime: <key>ctrl</key>]-<key>`</key>]
  - <key>ctrl</key>]v<key>`</key>] to paste into the text box at very bottom (status bar)
  - wait a few seconds then close sublime
  - relaunch sublime and pin it or shortcut it somewhere you can find it
  - wait a few seconds and
3. [sublime 3 linter](http://www.sublimelinter.com/en/latest/) should be installed by default, but if not...
  - Under Preferences menu, select `Package Control`
  - Type "install", select "install package" and hit <key>Enter</key>
  - Type "linter", select "sublime linter" and hit <key>Enter</key>
  - read the docs referred to in the README that pops up, or forge ahead...
4. Do **_NOT_** install [sublime-linter-pep8](https://github.com/SublimeLinter/SublimeLinter-pep8), the `Anaconda` PEP8 linter is better
  - `$ sudo apt-get install python3-pip`
  - `$ sudo pip3 install pep8`
  - Under Preferences menu, select `Package Control`
  - Type "install" and <key>Enter</key>
  - Type pep8 and <key>Enter</key>
5. Create a project
  - Project -> Add folders to project -> select your git repos, such ass...
    - `~/src/`?
    - `~/code/qary`?
    - `~/.config/sublime-text-2/Packages/User/` (old settings for copy pasta)
    - `~/Dropbox/notes` (all your journals and notes, so you can **stay sublime all day**)
  - save the project in e.g. `~/code/_sublime-projects/first-project.sublime-project`

In case you needed another reason to love Sublime... these instructions have remained almost exactly the same for the past 4 years.
I just needed to add the word **_NOT_** to one of the steps that the developer has made moot.
The Change Log for Sublime makes things better for the user, not for the company that built it.
And the company is just an individual developer in Sydney, Aus.

It has all the features that help him build great products... like Sublime.


## Auto Linting

Here are my config files in a tar.gz file you can extract to your `$HOME` to configure your system the same as mine [settings-sublime-text-3.tar.gz](https://tan.sfo2.digitaloceanspaces.com/config/settings-sublime-text-3.tar.gz).
You can automatically install it on your linux system by extracting the file to the path that Sublime (and other Linux applications) use to store config files.

```console
echo "WARNING: This will overwrite any settings you've already configured within `~/.config/sublime-text-3/Packages/User/` !!!!!!!!"
# You must manually uncomment this next line for this to work:
# curl https://tan.sfo2.digitaloceanspaces.com/config/settings-sublime-text-3.tar.gz | tar xvzC $HOME
```

Here are lists of all the things Anaconda autolinting can do for you:

### E1.. - Indentation

- E111 - Reindent all lines.
- E121 - Fix indentation to be a multiple of four.
- E122 - Add absent indentation for hanging indentation.
- E123,E124 - Align closing bracket to match opening bracket (E123) and aesthetics (E124).
- E125 - Indent to distinguish line from next logical line.
- E126 - Fix over-indented hanging indentation.
- E127,E128- - Fix visual indentation.
- E129 - Indent to distinguish line from next logical line.

### E2.. - Whitespace

- E201-E203,E211 - Remove extraneous whitespace.
- E221-E223 - Fix extraneous whitespace around keywords.
- E224 - Remove extraneous whitespace around operator.
- E225-E228 - Fix missing whitespace around operator.
- E231 - Add missing whitespace.
- E241 - Fix extraneous whitespace around keywords.
- E242 - Remove extraneous whitespace around operator.
- E251 - Remove whitespace around parameter '=' sign.
- E26  - Format block comments.
- E261 - Fix spacing before comment hash.
- E262 - Fix spacing after comment hash.
- E271-E274 - Fix extraneous whitespace around keywords.

### E3.. - Newlines

- E301 - Add missing blank line.
- E302 - Add missing 2 blank lines.
- E303 - Remove extra blank lines.
- E304 - Remove blank line following function decorator.

### E4+  - Other

- E401 - Put imports on separate lines.
- E501 - Try to make lines fit within `--max-line-length` characters.
- E502 - Remove extraneous escape of newline.
- E701-E703 - Put semicolon (E702,E703) or colon (E701) separated compound statement on separate lines.
- E711,E712 - Fix comparison with None (E711) or boolean (E712).

And here are the ones that I like to ignore:

```json
    /*
        A list of PEP8 error numbers to ignore.

        The complete list of PEP8 error codes is in this file:
            https://pycodestyle.readthedocs.io/en/latest/intro.html#error-codes.
            (Search that page for "E###:", where ### is a 3-digit number)

        E309: ignored by default as it conflicts with E211 described in PEP257
            Can't find E309 in pep8 docs! https://pep8.readthedocs.io/en/release-1.7.x/intro.html
    */
    "pep8_ignore":
    [
        "E226", // Fix missing whitespace around operator. I like `x**2` instead of `x ** 2`
        "E302", // Add missing 2 blank lines.
        "E309", // Ignored by default as it conflicts with PEP257 E211 (remove extraneous whitespace)
        "E123", // Align closing bracket to match opening bracket.
        "W503", // fix line break occurred before a binary operator (`and`, `or`, `+`, `**` etc)
        "W291" // Remove trailing whitespace at end of line.
    ],
```

Because Python code is not compiled, you miss out on a lot of compile-time error catching.
A linter can give you the benefit of a compiler without all the headaches.
It's super-helpful to be able to catch runtime errors with a linter.
Here are some of the potential runtime errors that Anaconda can warn you about, every time you save a file:

- Unused imports
- Missing imports or undefined variables and packages or `import *`

I can't wait until the Anaconda plugin does type checking (using type hints), like other typed languages and compilers.
