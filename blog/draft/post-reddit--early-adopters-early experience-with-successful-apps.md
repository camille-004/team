We're doing an IDP for a new messaging and social app for education. Seems like wildly successful and elegant user interfaces did it differently. Have you used or developed an awesome app in the early days? Was it awesome right away or did it get better with age/revision?

- Aug 2013 Release: [Slack](https://en.wikipedia.org/wiki/Slack_(software)#History): immediately addictive, seems to have feature bloat now?
- Sep 2012 Beta: [Zoom](https://en.wikipedia.org/wiki/Zoom_(software)#History): Loved it immediately, even the vanity touch-up feature
- Aug 2009 Beta: [WhatsApp](https://en.wikipedia.org/wiki/WhatsApp#History): I was late to the party. Like it OK now for digital nomading.

Gmail? Coinbase? Twitch? TikToc? Reddit?
