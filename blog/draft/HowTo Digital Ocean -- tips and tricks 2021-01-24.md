# Digital Ocean

## Destroying Droplet

To stop being billed for a Droplet it needs to be destroyed (not just turned off):

1. turn it off with switch from droplet page
2. take snapshot (takes a long time)
3. destroy

## Docker Containers in Droplet

1. spin up a oone-click droplet with Docker
2. ssh into droplet using token and install app using docker (running as root)

## Spaces

- use hyphens or `_`, avoid spaces in paths (folder or file names)
- start file names with broad categories and work your way down to the participants/authors names and finally the date
- use `-` or `--` to separate "folder" names, that way you don't have to even specify directories, making the path easier to remember and files easier to find
- operations may never complete and files may be duplicated if you do not purge CDN cache before renaming or moving a file
