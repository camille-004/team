# fake-news-detection
NLP project for Tangible AI

Dataset can be found here:
https://www.kaggle.com/clmentbisaillon/fake-and-real-news-dataset

## TODO
- Create data preparation pipeline
- Fix removing skewed words (`combined_no_skew`)
