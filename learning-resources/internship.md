# Internship

## Commitments

- [ ] time commitment: 10 hr/wk for 10 weeks (one academic quarter)
    - [ ] Tangible AI meetings (approx 1 hr/wk)
    - [ ] self-directed "capstone" project (approx 9 hr/wk)
        - [ ] anything open source: data science, web dev, NLP, CV, or python  
- [ ] work remotely with other interns and developers at Tangible AI (Agile process)
    - [ ] access to the Tangible AI slack workspace (help from others)
    - [ ] _optional_: twice weekly 15 minute standup status meetings 
    - [ ] _optional_: weekly mob programming session including monthly retrospective and sprint planning
    - [ ] weekly 30 minute 1:1 with me or Maria, Dwayne, or a core team member (change at any time)
- [ ] 4 deliverables:
    - [ ] graded conversational quiz (qary -s quiz) at start of internship
    - [ ] an 8 week project plan (abstract) within first 2 weeks
    - [ ] graded quiz at the end of your internship to quantify your progress
    - [ ] report to the team on your project - code walkthrough or slidedeck or blog post

## Benefits

- [ ] access to Tangible AI staff and learning resources for life
- [ ] free ebook _Natural Language Processing in Action_
- [ ] access to GPU servers on as-needed basis
- [ ] access to cloud servers (botpress, rasa, nginx, django, postgress) on as-needed basis
- [ ] LinkedIn/GitLab mentions, connections, endorsements, as appropriate
- [ ] cross-linking to personal website, as appropriate
- [ ] employment references and recommendations, as appropriate
- [ ] "Tangible AI ML/Python/DS/NLP Engineering Intern" title on Linked In

## User Story

What are the practical skills that interns would like to take away from an unpaid educational internship?

* Programming basics:
    * Importing modules
    * conditions
    * loops
* Basic ML understanding (also AI, from big picture perspective)
* Basic web development (HTTP requests, REST APIs, JSON)
* Coding best practices, patterns, anti-patterns, style, linters, and auto-formatters
* Basic of chatbot building: intents, entities, utterances

* Company and vision:
    * Tangible AI's goals, values, and business model
    * "How nonprofits use AI" webinar
    * Open source principles
    * Open source licenses
    * Prosocial AI

## Checklist & Syllabus

This checklist is interactive within gitlab, I think.
So students can keep track of their progress within a fork of this repository.
And a bot may be able to follow their progress by tracking their checkboxes.

Here's a draft schedule of what you can expect for the next 10 weeks: [internship-syllabus.md]

## Project Ideas (10 weeks)

### Basic

- Twitter ad scraper targeting nonprofits (hardcoded list of nonprofit account @handles)
    - [How to scrape tweets from Twitter](https://towardsdatascience.com/how-to-scrape-tweets-from-twitter-59287e20f0f1)
- elasticcloud as repository for scraped tweets
- Tweet scraper targeting hard-coded list of nonprofit relevant hashtags
- named entity extraction and identification from personal tweets (from list of small nonprofits)
- bot and ad filtering/identificaiton
- sentiment analysis
- summarization of sentiment for each nonprofit brand
- topic analysis for each nonprofit brand
- twitter search from within qary? e.g. user: "tweets about Red Cross?" qary: "sentiment is 75% positive with a trending topic 'donate blood for fires in California'"

### Computer Science

- spelling corrector
- GED, GEC, GEH
- dialog planner to coach ESL learners in composing correct sentences (Mohammed's webapp)
- Drawing/listening ESL games that could be autmated with a chatbot:
    - [ESL Listening Games](2020-09-esl-writing-skill-coach-high-schoolers.md)
- Grammar correctors/suggester like Grammerly, but we'd like to be more conversational, like a parent or teacher
    - [Free Writing Assistant for Google Docs by ETS (SAT Test prep)](https://mentormywriting.org/)

### ML/NLP

- NLP within qary?
- context planner?
- actions/commands/reminders in qary?
- LSTM/GRU/Transformer to translate English->French/Spanish or rare language like Amharic

### Chatbot Converter (Urgent Need)

#### translate `rapidpro.json` into  `botpress.json`

- RapidPro is designed for SMS so doesn't have real buttons, like botpress.
- Only Rapidpro `message node`s and `router node`s need to be translated
    - with output destination (platform, user) pair of IDs
    - with buttons (called quick-replies in rapidpro)
- Router
    - `has words` intent classifier
    - stretch: `regex` intent classifier
- stretch: intent recognition

#### Tasks:

- [ ] build same simple 2-node (mulitple-choice node in rapidpro) bot with button in each
  - [ ] botpress (`choice`) - 2 pt
  - [ ] rapidpro (`ready-reply`) - 2 pt
- [ ] export exmple of each for choice/reply feature - 2 pt
- [ ] understand schema of each for choice/reply feature, [here]()'s a video walkthrough of rapidpro's schema - 4 pt
  - [ ] change something about each bot and re-export (active learning) - 1 pt
- [ ] json loader for both - .5 pt
- [ ] python func updates empty `botpress.json` schema with list of dicts from rapidpro.json (1 multichoice node)
- [ ] python func updates list of single multichoice node to connect a node to one of the branches in the root multichoice node

## Winter 2021 Projects

### Billy

- [x] Datacamp capstone: Image Classifier Django app for recyclables
- [ ] Deploy and productize the Django App
- [ ] Object detector

### Winston

- [ ] Machine Learning to predict lottery wins for passes to backpack in Enchantments National Forest
- Website to allow pass applicants to select the best parameters for their application
- [ ] Add weather/climage features (ave temp, precip, typical low/high/median temp)
- [ ] Add datetime features (month, week, holiday, weekend, DOW, week of holiday, weekend of holiday)
- [ ] Simulation of competitive dynamics (economics)
- [ ] Solve for equilibrium (perhaps dynamic equilibrium rather than static)

### Hanna

- Machine Translation English<->French
- Machine Translation English<->Amharic

### Jose

- Quiz bot for interns

### John

- Realtime chatbot with Vue.js or Svelte.js and RabbitMQ + Celery?

### Uzi

### Polina

- Wireframes for teacher dashboard

### Winnie

- NMT package
- NLPIA content ch 10
- blog post on Sublime etc


