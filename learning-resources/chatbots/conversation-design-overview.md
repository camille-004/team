# Conversation UX Design

Maria Dyshel (CEO, Tangible AI) put together some elegant [graphics of the conversation design process](https://docs.google.com/document/d/1anmgpugcApoeRO36ed1Eznq0V8ZgxwoZkb5vkLp45Eo/edit?usp=sharing) and a video explaining conversation design principles (ask her where you can find it).

*Conversation UX design* is like UX design for most other apps. 
You must understand your users' and stakeholders' needs and coallesce those needs into a feasible design:

1. interview stakeholders
2. interview users
3. develop a user persona (with stakeholders)
4. select and design features (with engineers)
5. requirements development (with engineers)

### Conversation Principles

- Accurate
- Appropriate
- Length
- Relevant

You probably know a lot about these steps. 
The challenging (and powerful) aspect of conversation UX design is that the UI is limited to a voice or chat interface. 
You are designing a *conversational agent*, a *dialog system*, or a *chatbot*.
These all mean the same thing. 
Your designing a computer program to engage in conversational dialog with your user.

For many chatbot platforms and interfaces, you will not get to customize the buttons and other visual cues.
Conventional graphical design affordances may not be an option for you.
Your main UX tool is usually the natural language text (often English) that you compose for the chatbot to *utter* to your users.
You also get to directly enter all the things you anticipate your users might say to the bot.

This is a powerful paradigm shift in app development.
It means you can implement your ideas directly into the *dialog system* without writing a line of code.
You only need to change the content (the words) within a text file to immediately see your ideas come to life in a chatbot.
This gives whole new meaning to the idea of rapid prototyping and Agile development.

Composing a chatbot is now as easy as composing the HTML for a website using a drag-and-drop GUI like Wix or a WSIWYG editor like Wordpress.
In fact there's an open source chatbot platform and framework called *Botpress* that imitates this Wordpress approach for chatbots.
More on that (and *Rasa*) later.

Though a conversation engine's capabilities are only limited by the content you can write in the data files, your user's requests are also limitless.
But user testing can usually uncover edge cases that you were unable to imagine during the design phase.
However, this explosive growth in complexity with every turn of the dialog means that you will quickly graduate from the GUI. 
You will find it's much easier to maintain a large, complex chatbot if you take advantage of the tools programmers use to manage large software projects.
Text files in a structured hierarchy of folders can help.
Version control systems are another big improvement over what's possible within a GUI.
And code/dialog review within a git GUI such as gitlab.com can also be a huge boost in productivity and the quality of the bots you design.

Designing the UX of a conversational agent or dialog system is also sometimes referred to as voice-first design. 
This is because the interface to the chatbot is often a speech recognition system and a voice synthesis system.
But you'll first want to understand the capabilities and limitations of a chatbot before you dig into the complexities of voice interface design.

## Chatbot Structural Design

In order to understand all the ways users' needs might be satisfied with a chatbot you must first understand what a *dialog system* can and cannot do. 
This is what you will do here as you learn how to design the structure of a conversation.
And this will require a bit of understanding about *Natural Language Processing* (NLP). 

### Guard Rails

As you learned earlier, the things humans might say is limitless. 
So rules-based bots have must put guard rails on the conversation.
Conversation guard rails keep the conversation flowing within the thick middle of the dialog tree.
This is where the bot is smartest.
This is where it has something interesting to say. 

- "I don't know. I get my information from wikipedia, and I can't find much about your question."
- "I don't understand. Could you say that a different way?"
- "Tell me more."
- "That's out of my wheelhouse. I'm really curious about you. Where did you hear about that?"
- "I can't understand you but I like you. Tell me more about you."
- "I'm not very smart yet. I can only talk about things like ..."



This poses unique UX design challenges.
The main challenge is to provide affordances and "guuard rails" or "guide rails" to constrain the otherwise unlimited ways in which users may chat with a conversation interface.

*Conversation structure design* or *conversation engineering* is the crafting of content within a data structure that can be executed by a machine.
This data structure is then "run" like a python script, to enable the machine to have a conversation with a human.
In the early days, only software engineers were able to create dialog engines using programming languages like C and Python.
In the 20th century, however, data structures like AIML and other chatbot languages were invented that allowed even nonprogrammers to build sophisticated chatbots.

## References

- Intro to Conversation Design [slides](https://docs.google.com/document/d/1anmgpugcApoeRO36ed1Eznq0V8ZgxwoZkb5vkLp45Eo/edit?usp=sharing) and _video_ by Maria Dyshel, CEO, Tangible AI
- [NLP Chapter 24](https://web.stanford.edu/~jurafsky/slp3/24.pdf) by Jurafsky (Stanford)
- [qary.skills.quiz]() by Jose Robins
- Your first Rasa chatbot [video](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-build-your-first-rasa-chatbot-hobson-eda-50min-2021-02-15.mp4) and [instructions](./your-first-rasa-chatbot.md) by Hobson Lane and Eda Firat
- Install Rasa the easy way [video](https://tan.sfo2.digitaloceanspaces.com/videos/howto/onboarding-howto-install-rasa-easy-nondeveloper-way-also-conda-create-environment-and-python-paths-using-bash-which-python-pip-tilde-by-hobs-eda-2020-12-31.mp4) by Hobson Lane and Eda Firat
- How to install qary  [video](https://tan.sfo2.digitaloceanspaces.com/videos/howto/onboarding-how-to-install-qary-john-2020-11-20.mp4) by John May

## Terminology

**dialog tree**: A directed graph of connections between the states of a chatbot or dialog engine or conversation design. In programming lingo these are sometimes referred to as Finite State Machines or Finite State Transducers.
**domain specific language** (DSL): A language or data structure intended to be run or executed by a specialized software program called an interpretter or compiler. Some examples you may have heard of include Markdown, HTML, and YAML.
**intent**: A particular action or utterance that a human desires to invoke in the chatbot. In conversation engineering, a list of intents represents forks in a dialog tree.
**natural language**: Sequences of symbols (language) used by humans or other animals in their communication with each other. The word "natural" is intended to distinguish natural languages from programming languages for computers. Some examples of natural languages include English, Spanish, Amharic, Navaho, Chinese, and even whistling or bird song.
**natural language processing** (NLP): Computing some output value based on a natural language sequence. A chatbot or dialog system is one such processor for natural language intended to output or generate natural language that responds to a user's text messages. NLP is often divided into *natural language understanding* (NLU) and *natural language generation* (NLG).
**natural language understanding**: Computing the user's intent or sentiment based on the text of their message or utterance.
**natural language generation**: Computing a sequence of natural language words to convey some though or emotion to the recipient of the text message.
**phone tree**: An automated telephone answering service widely used as far back as the 80's and 90's. It works with land line telephone systems as well as mobile phones using the touch-tone system where users press numbers on their phone. Example: "Press 1 for English, 2 for Spanish, 0 for the main menu".
**state**: A mode of operation that a machine switches too with each utterance by the user. Within a particular state, the machine will only react to the list of messages or *intent*s from the user that were assigned to that *state* by the conversation designer.
**story**: Rasa term for a chain or tree of turns -- sequence of potential interactions between a bot and a human.
**turn**: A pair of chat messages or utterances from two parties in a conversation
**utterance**: A chat message from a bot or a human -- from the word "utter", which means to say
