# Internship Syllabus

## Skill Checklist

You'll learn these skills over the 10 weeks of this course, but not necessarily in this order. For example the Python skills will be spread across all 10 weeks.

* 01: Programming (Python):
    * [ ] install packages from pypi 
    * [ ] importing modules within packages like sklearn and Pandas
    * [ ] install packages from source code (--editable)
    * [ ] install and import packages from gitlab
    * [ ] conditions: `if`, `else`
    * [ ] loops: `for`, `while`
    * [ ] functions (args, kwargs, `return`)
    * [ ] classes: `class`, methods, attributes
    * [ ] scalar numerical data types (`int`, `float`)
    * [ ] sequence data types (`str`, `bytes`, `list`, `tuple`)
    * [ ] mapping (`dict`, `Counter`)
    * [ ] sets (`set`)
    * [ ] create stackoverflow account
    * [ ] reading Tracebacks and error messages
    * [ ] getting help (`help()`, `?`, `??`, `[TAB] completion`, `vars()`, `dir()`, `type()`, `print()`, `.__doc__`)
    * 
* 02: Shell (Bash)
    * [ ] bash nlp (`wc`, `grep` [stanford.edu/class/cs124/lec/textprocessingboth.pdf])
    * [ ] navigate directories (`cd`, `pwd`, `ls -hal`, `/`, `~`, `*`, `$HOME`)
    * [ ] manipulate files + directories (`mkdir ~/code`, `mv`, `cp`, `touch`)
    * [ ] work with text files (`more`, `cat`, `nano`)
    * [ ] pipes and redirects (`|`, `>`, `>>`, `<`)
    * [ ] processes (`ps aux`, `fg`, `bg`, `&`)
    * [ ] conditions (`&&`, `||`)
    * [ ] tab-completion
    * [ ] comments and shabang (`# `, `#!`)
    * [ ] permissions (`chmod`, `chown`)
    * [ ] running shell commands (`source`, `.`, `eval`)
    * [ ] finding files: `find . -iname qary -size +1k`
    * [ ] ssh to remote server: `ssh intern@totalgood.org`
    * [ ] set up ssh keys: `ssh-keygen`, `ssh-copy-id`
    * 
* 03: Git
    * [ ] create gitlab account
    * [ ] find and fork a project on gitlab.com
    * [ ] create project in gitlab.com
    * [ ] edit a file in gitlab.com (README.md)
    * [ ] add a file using gitlab.com GUI
    * [ ] upload a file to gitlab.com with GUI
    * [ ] ssh public key in gitlab
    * [ ] clone a repository from the command line 
    * 
* 04: Data
    * [ ] loops: `enumerate`, `tqdm`
    * [ ] list comprehensions to transform features: `[x**2 for x in array]`
    * [ ] conditional list comprehension: `[x**2 for x in array if x > 5]`
    * [ ] load csv: `df = pd.read_csv()`,  `df = pd.read_csv(sep='\t')`
    * [ ] DataFrames from HTML tables: `df = pd.read_html()`
    * [ ] bigdata: `df = pd.read_csv(chunk_size=...)`
    * [ ] vectorized operations: `x1 + x2` 
    * [ ] concatenate: `pd.concat(axis=0/1)`
* 05: Statistics 
    * [ ] `np.random.rand`, `.randint`, `.randn`, `np.random.seed`
    * 
* 06: Basic AI understanding
    * [ ] make predictions/estimates with pretrained machine learning model
    * [ ] training/fitting a machine learning model on a training set
    * [ ] validating/evaluating/testing a model on a validation set or test set
    * [ ] overfitting: how to detect it, and what to do about it
    * [ ] class bias: how to detect it, and what to do about it
    * 
* 07: Networking
    * [ ] python `requests` package and `.get`, `.post` methods and `stream=True`
    * [ ] `pandas.read_html()` function
    * [ ] python `BeautifulSoup4` package
    * [ ] address bar in browser for HTTP GET requests with arguments
    * [ ] address bar in browser for HTTPS requests with arguments
    * [ ] `wget` or c`url` for command line GET requests with arguments
    * [ ] `wget` or `curl` for command line POST requests with data (arguments)
    * 
* 08: Coding style
    * [ ] PEP8
    * [ ] linters
    * [ ] Syntax highlighters
    * [ ] [Napolean-style docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)
    * [ ] [doctests](https://django-testing-docs.readthedocs.io/en/latest/basic_doctests.html)
    * [ ] Anaconda auto-formatter for Sublime
    * 
* 09: NLP and chatbot building:
    * [ ] chatbot architectures (deterministic, generrative, knowledge-based)
    * [ ] intents and intent recognition
    * [ ] information extraction, entities
    * [ ] search
    * [ ] utterances
    * [ ] conversation management
    * [ ] voice recognition and multimodal UI (AllenAI)

* Company and vision:
    * Introduction to Tangible AI
    * "How nonprofits use AI" webinar
    * Open source principles
    * Open source licenses and how to choose the right one

## Broadcast messages

```yaml
-
    message: |+
        Here are some Slack workspaces you'll probably want to join:
            * Tangible AI (#interns): https://join.slack.com/t/tangibleai/shared_invite/zt-kvci7k5e-9xMi5viwPBfQvpMhyHRbFA
            * SD Machine Learning (#qary & #nlpia): https://join.slack.com/t/sdmachinelearning/shared_invite/zt-6b0ojqdz-9bG7tyJMddVHZ3Zm9IajJA
            * SD Python User Group: https://join.slack.com/t/pythonsd/shared_invite/zt-l6tks202-WJY~ycxpRx~vzQYSHFQHWw
    day: 0
-
    message: |+
        Are meetups which can be a fun way to learn in a social way. I listen in  every saturday to both of them. And I often present at the python user group monthly Thursday night meetup. This would be a great place for you to present your project:
            * SDML Meetups are an awesome way to learn: https://www.meetup.com/San-Diego-Machine-Learning/
            * SD Python study group saturday morning is great too: https://www.meetup.com/pythonsd/
    day: 7
```
## Resources

- ["Querying JSON/YML/XML files from the command line"](http://hackerpublicradio.org/eps.php?id=3252) (15 min) by [CRVS](http://hackerpublicradio.org/correspondents.php?hostid=385) on Hacker Public Radio Episode 3252
    - short introduction to json file format
    - 3 cool bash tools for parsing yaml, json, and even xml files.
    - You can "query" the yml/json/xml files like a database so the apps called `yq`, `jq`, and `xq`
    - They also let you convert between each format

- ["How Python Makes Thinking in Code Easier"](https://blog.dropbox.com/topics/work-culture/-the-mind-at-work--guido-van-rossum-on-how-python-makes-thinking)
